package net.javaguides.genericdao;

import net.javaguides.usermanagement.model.User;
import net.javaguides.usermanagement.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.lang.reflect.ParameterizedType;
import java.util.function.Consumer;

public class GenericDao<T> {
    /**
     * Save Entity
     * @param entity
     */
    public void save(T entity) {
        operation(session -> session.save(entity));
    }

    /**
     * Update Entity
     * @param entity
     */
    public void update(T entity) {
        operation(session -> session.update(entity));
    }

    /**
     * Delete Entity
     * @param entity
     */
    public void delete(int entity) {
        Class<T> type = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

        operation(session -> session.delete(session.get(type, entity)));
    }

    protected void operation(Consumer<Session> operation) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object

            operation.accept(session);

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

}
