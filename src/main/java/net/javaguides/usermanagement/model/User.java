package net.javaguides.usermanagement.model;

import net.javaguides.emeialmanagement.model.EmailAddress;

import javax.persistence.*;

/**
 * User.java
 * This is a model class represents a User entity
 *
 * @author Ramesh Fadatare
 */

@Entity                 // mówi o tym że działamy na bazie danych
@Table(name = "users")    // jaka będzie tabela
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // jeżeli będziemy zakładac now eobiekty nie trzeba podawać ID
    @Column(name = "id")
    protected int id;

    @Column(name = "name")
    protected String name;

    @Column(name = "email")
    protected String email;

    @OneToOne
    EmailAddress emailAddress;


    public User() {
    }

    public User withId(int id) {
        setId(id);
        return this;
    }

    public User withName(String name) {
        this.name = name;
        return this;
    }

    public User withEmail(String email) {
        this.email = email;
        return this;
    }

    public User build() {
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}