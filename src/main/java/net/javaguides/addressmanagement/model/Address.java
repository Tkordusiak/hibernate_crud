package net.javaguides.addressmanagement.model;

import javax.persistence.*;

/**
 * User.java
 * This is a model class represents a User entity
 * @author Ramesh Fadatare
 *
 */

@Entity
@Table(name="addresses")
public class Address {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="country")
    protected String country;

    @Column(name="city")
    private String city;

    @Column(name="street")
    private String street;

    @Column(name="postalCode")
    private String postalCode;

    @Column(name="number")
    private String number;

    public Address() {
    }

    public Address withCountry(String country){
        setCountry(country);
        return this;
    }
    public Address withCity(String city){
        setCity(city);
        return this;
    }
    public Address withStreet(String street){
        setStreet(street);
        return this;
    }
    public Address withPostalCode(String postalCode){
        setPostalCode(postalCode);
        return this;
    }
    public Address withNumber(String number){
        setNumber(number);
        return this;
    }

    public Address withId(int id){
        setId(id);
        return this;
    }

    public Address build(){
        //actions on properties
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}