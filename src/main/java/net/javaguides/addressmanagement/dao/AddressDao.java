package net.javaguides.addressmanagement.dao;

import net.javaguides.addressmanagement.model.Address;
import net.javaguides.genericdao.GenericDao;
import net.javaguides.usermanagement.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
/**
 * CRUD database operations
 * @author Ramesh Fadatare
 *
 */
public class AddressDao extends GenericDao<Address> {

    /**
     * Get Address By ID
     * @param id
     * @return
     */
    public Address getAddress(int id) {

        Transaction transaction = null;
        Address address = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an address object
            address = session.get(Address.class, id);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return address;
    }

    /**
     * Get all Addresss
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < Address > getAllAddress() {

        Transaction transaction = null;
        List < Address > listOfAddress = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an address object

            listOfAddress = session.createQuery("from Address").getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfAddress;
    }

}