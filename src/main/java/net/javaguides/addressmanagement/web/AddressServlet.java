package net.javaguides.addressmanagement.web;

import net.javaguides.addressmanagement.dao.AddressDao;
import net.javaguides.addressmanagement.model.Address;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * ControllerServlet.java
 * This servlet acts as a page controller for the application, handling all
 * requests from the user.
 * @email Ramesh Fadatare
 */


@WebServlet("/addresses")
public class AddressServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private AddressDao addressDao;

    public void init() {
        addressDao = new AddressDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        if(action == null){
            action = "/list";
        }

        try {
            switch (action) {
                case "new":
                    showNewForm(request, response);
                    break;
                case "insert":
                    insertUser(request, response);
                    break;
                case "delete":
                    deleteUser(request, response);
                    break;
                case "edit":
                    showEditForm(request, response);
                    break;
                case "update":
                    updateUser(request, response);
                    break;
                default:
                    listAddress(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listAddress(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Address> listUser = addressDao.getAllAddress();
        request.setAttribute("listAddress", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("address-list.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("address-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Address existingUser = addressDao.getAddress(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("address-form.jsp");
        request.setAttribute("address", existingUser);
        dispatcher.forward(request, response);

    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String postalCode = request.getParameter("postalCode");
        String street = request.getParameter("street");
        String number = request.getParameter("number");
        net.javaguides.addressmanagement.model.Address address = new net.javaguides.addressmanagement.model.Address()
                .withCountry(country)
                .withCity(city)
                .withPostalCode(postalCode)
                .withStreet(street)
                .withNumber(number)
                .build();
        addressDao.save(address);
        response.sendRedirect("addresses");
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String postalCode = request.getParameter("postalCode");
        String street = request.getParameter("street");
        String number = request.getParameter("number");

        net.javaguides.addressmanagement.model.Address address = new net.javaguides.addressmanagement.model.Address()
                .withId(id)
                .withCountry(country)
                .withCity(city)
                .withPostalCode(postalCode)
                .withStreet(street)
                .withNumber(number)
                .build();

        addressDao.update(address);
        response.sendRedirect("addresses");
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        addressDao.delete(id);
        response.sendRedirect("addresses");
    }

}