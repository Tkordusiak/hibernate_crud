<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Address Management Application</title>
</head>
<body>
<center>
    <h1>Address Management</h1>
    <h2>
        <a href="addresses?action=new">Add New Address</a>
        &nbsp;&nbsp;&nbsp;
        <a href="addresses">List All Addresss</a>

    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of Addresss</h2></caption>
        <tr>
            <th>ID</th>
            <th>Country</th>
            <th>City</th>
            <th>Postal Code</th>
            <th>Street</th>
            <th>Number</th>
        </tr>
        <c:forEach var="address" items="${listAddress}">
            <tr>
                <td><c:out value="${address.id}" /></td>
                <td><c:out value="${address.country}" /></td>
                <td><c:out value="${address.city}" /></td>
                <td><c:out value="${address.postalCode}" /></td>
                <td><c:out value="${address.street}" /></td>
                <td><c:out value="${address.number}" /></td>
                <td>
                    <a href="?action=edit&id=<c:out value='${address.id}' />">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<c:out value='${address.id}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>

